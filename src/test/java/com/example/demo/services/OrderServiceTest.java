package com.example.demo.services;

import com.example.demo.dtos.ClientDto;
import com.example.demo.dtos.OrderDto;
import com.google.common.hash.Hashing;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.nio.charset.StandardCharsets;

import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
class OrderServiceTest {

    @Autowired
    private OrderService orderService;

    @MockBean
    private MeterRegistry meterRegistry;

    @Test
    void testOrder() {
        // Arrange.
        ClientDto client = ClientDto.builder()
                .firstname("Joe")
                .lastname("Doe")
                .address("France")
                .build();

        OrderDto order = OrderDto.builder()
                .name("Chaussette")
                .client(client)
                .build();

        String hash = this.makeHash(order);
        Mockito.when(
            meterRegistry.counter(any(String.class), any(String.class), any(String.class))
        ).thenReturn(Mockito.mock(Counter.class));

        // Act.
        String output = this.orderService.order(order);

        // Assert.
        Assertions.assertThat(output).isEqualTo(hash);
    }

    private String makeHash(OrderDto order) {
        String content = String.format(
                "%s%s%s",
                order.getName(),
                order.getClient().getFirstname(),
                order.getClient().getLastname()
        );

        return Hashing.sha256()
                .hashString(content, StandardCharsets.UTF_8)
                .toString();
    }
}
