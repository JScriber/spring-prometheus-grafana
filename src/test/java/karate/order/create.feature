Feature: Create orders ...

  Background:
    * url baseUrl

  Scenario: Add an order

    Given path '/orders'
    And request { name: 'Chaussettes', client: { firstname: "Tintin", lastname: "Titi", address: "France" } }
    And header Accept = 'application/json'
    When method post
    Then status 200
    And match response == '8cad4aab15803748d893febbc7ddae395011f15e6d91b19a61536fb6faf5fe08'
