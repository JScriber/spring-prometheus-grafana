package com.example.demo.services;

import com.example.demo.dtos.OrderDto;
import com.google.common.hash.Hashing;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Service
public class OrderService {

    private final MeterRegistry meterRegistry;

    public OrderService(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    public String order(OrderDto order) {
        meterRegistry.counter("orders", "name", order.getName()).increment();
        meterRegistry.counter("address", "name", order.getClient().getAddress()).increment();

        return makeOrderNumber(order);
    }

    private String makeOrderNumber(OrderDto order) {
        String content = String.format(
                "%s%s%s",
                order.getName(),
                order.getClient().getFirstname(),
                order.getClient().getLastname()
        );

        return Hashing.sha256()
                .hashString(content, StandardCharsets.UTF_8)
                .toString();
    }
}
