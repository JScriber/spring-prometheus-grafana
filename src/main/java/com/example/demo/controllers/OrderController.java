package com.example.demo.controllers;

import com.example.demo.dtos.OrderDto;
import com.example.demo.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @Autowired
    private OrderService service;

    @PostMapping("/orders")
    public String order(@RequestBody OrderDto order) {
        return service.order(order);
    }

}
