package com.example.demo.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientDto {
    private String firstname;
    private String lastname;
    private String address;
}
