package com.example.demo.dtos;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class OrderDto {
    private String name;
    private ClientDto client;
}
