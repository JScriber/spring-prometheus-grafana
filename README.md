# Docker

```
docker network create --driver bridge spring-prometheus-grafana
```

## Build grafana

```
docker build -t spring-grafana .
docker run -d \
-p 3030:3000 \
--name spring-grafana \
--network spring-prometheus-grafana \
--rm spring-grafana
```

## Build prometheus

```
docker build -t spring-prometheus .
docker run -d -p 9090:9090 --name spring-prometheus --network spring-prometheus-grafana --rm spring-prometheus
```

## Build project

```
./mvnw clean package
docker build -t spring-prometheus-grafana .
docker run -d \
-p 8080:8080 \
--name spring-prometheus-grafana \
--network spring-prometheus-grafana \
--rm spring-prometheus-grafana
```

# Karate

Run project:
```
./mvnw spring-boot:run
```

Run order test
```
./mvnw test -Dtest=KarateTest -Dkarate.host=http://localhost:8080
```
